//
//  CircuitMap.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 29/11/2021.
//

#include "CircuitMap.hpp"
#include "CircuitElement.hpp"

CircuitMap::CircuitMap(): size(0), height(0), width(0), initialised(false) {
}

CircuitMap::~CircuitMap() {
    delete [] this->map;
    this->map = nullptr;
}


void CircuitMap::put(CircuitElement* element, int x, int y) {
    // if x is out of the defined size skip put
    if (x < 0 || x > this->width - 1) {
        return;
    }
    
    // if y is out of defined size then skip put
    if (y < 0 || y > this->height - 1) {
        return;
    }
    
    // after checks passed put the node pointer in the correct map position
    this->map[y * this->width + x] = element;
}


CircuitElement* CircuitMap::get(int x, int y){
    // if x is out of the defined size skip get
    if (x < 0 || x > this->width - 1) {
        return nullptr;
    }
    
    // if y is out of defined size then skip get
    if (y < 0 || y > this->height - 1) {
        return nullptr;
    }
    
    return this->map[y * this->width + x];
}


void CircuitMap::initialise(int width, int height) {
    // check wether initialisaton already occured, if so skip the function
    if (this->initialised) {
        return;
    }
    
    // set initialisation status to true
    this->initialised = true;
    
    // initialise size variables
    this->width = width;
    this->height = height;
    this->size = width*height;
    
    // make circuit map full of null pointers
    this->map = new CircuitElement*[this->size]();
    
    for (int i = 0; i < this->size; i++) {
        this->map[i] = nullptr;
    }
    
}
