//
//  CircuitMap.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 29/11/2021.
//

#ifndef CircuitMap_hpp
#define CircuitMap_hpp

#include "CircuitElement.hpp"

/// Consists of a array containing pointers to circuit elements in their respective position
///
/// Pointer elements are accesable via put and get methods
class CircuitMap {
private:
    /// Underlying data store where the pointers are stored
    ///
    /// This is allocated on the heap hece it has to be allocated and disallocated
    CircuitElement** map;
    
    
    /// Width of the circuit map
    int width;
    
    /// Height of the circuit map
    int height;
    
    /// Total numbre of elements of teh circuit map
    int size;
    
    /// Checks wether the datastructure was initialised and heap memory allocated, this can be done only once
    bool initialised;
    
    
public:
    /// Allocate heap memory for the elements
    CircuitMap();
    
    /// Deallocate memory created on heap
    ~CircuitMap();
    
    // TODO: include overloading for Coordinate class
    
    /// Writes the pointer to specified coordinate in the circuit map
    /// @param element pointer to the element that is going to be placed
    /// @param x  x position
    /// @param y  y position
    void put(CircuitElement* element, int x, int y);
    
    /// Gets a reference to the circuit element present at the cooridnate
    /// @param x  x position
    /// @param y  y position
    /// @return returns the reference of a circuit element at specified coordinate
    CircuitElement* get(int x, int y);
    
    /// Initialises the underlying data type by allocating heap memory
    /// @param width width of the underlying array
    /// @param height height of the underlying array
    ///
    /// Can be called only once, subsequent calls won't do anything
    void initialise(int width, int height);
};
#endif /* CircuitMap_hpp */
