//
//  Circuit.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 26/11/2021.
//

#ifndef Circuit_hpp
#define Circuit_hpp

#include "GraphicsElement.hpp"
#include "Screen.hpp"
#include "CircuitMap.hpp"
#include "Node.hpp"
#include "Resistor.hpp"
#include <list>

class Circuit: public GraphicsElement {
private:
    
public:
    /// Circuit map is a representation of the circuit in 2d. Each cell contains a pointer towards a circuit element
    ///
    /// Note that this is safe since class circuit owns vector of circuit lements hence it can never be disalocated
    CircuitMap map;
    
    /// Stores all the nodes of an electrical network
    std::list<Node> nodes;
    
    /// Stores all the resistors of a network
    std::list<Resistor> resistors;

    Circuit(Screen& screen);
    
    void draw() override;
    
    void load(const char* fileName);
    
    void simulate();
};


#endif /* Circuit_hpp */
