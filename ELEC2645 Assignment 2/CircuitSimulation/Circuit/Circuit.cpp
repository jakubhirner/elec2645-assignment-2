//
//  Circuit.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 26/11/2021.
//

#include "Circuit.hpp"
#include "Screen.hpp"
#include "GraphicsElement.hpp"
#include "CircuitLoader.hpp"
#include "Math.hpp"
#include <iostream>

Circuit::Circuit(Screen& screen): GraphicsElement(0, 0, screen) {
    
}

void Circuit::load(const char* fileName) {
    CircuitLoader loader(fileName, *this);
}

void Circuit::simulate(){
    int matrixSize = (int) this->nodes.size() - 1;
    // condtruct conductance matrix and set to zeré
    Matrix G(matrixSize,matrixSize);
    
    // construct current vector and set to zero
    Vector i(matrixSize);
    
    // define vector order in the matrix and array
    int index = 1; // index where to put order
    for (auto& node: nodes) {
        if (!node.isGround) {
            node.order = index;
            // set the current vector to 0
            // this can be done using the voltage setting of each node since all voltage nodes are identified to 0 unless it is a power node
            // but power nodes require the current vector to be equal to their voltage anyway
            i.set(index,node.voltage);
            index++;
        }
    }

    // construct the matrix
    // won't work if single resistor is connected to the same nodes
    for (auto& resistor: this->resistors) {
        float conductance = resistor.getConductance();
        int orderRight = resistor.right->order;
        int orderLeft = resistor.left->order;
        
        // left
        if (resistor.left->isGround) {
            // nothing
        } else if (resistor.left->isPower) {
            G.set(orderLeft,orderLeft,1);
        } else {
            if (resistor.right->isGround) {
                //ignor
            } else {
                float newConductance = G.get(orderLeft, orderRight) + conductance;
                G.set(orderLeft,orderRight,newConductance);
            }
            
            float newConductance = G.get(orderLeft,orderLeft) - conductance;
            G.set(orderLeft,orderLeft,newConductance);
        }
        
        // right
        if (resistor.right->isGround) {
            // nothing
        } else if (resistor.right->isPower) {
            G.set(orderRight,orderRight,1);
        } else {
            if (resistor.left->isGround) {
                //ignor
            } else {
                float newConductance = G.get(orderRight,orderLeft) + conductance;
                G.set(orderRight,orderLeft,newConductance);
            }
            float newConductance = G.get(orderRight,orderRight) - conductance;
            G.set(orderRight,orderRight,newConductance);
        }
    }
    
    // construct current vector and set to zero
    Vector result = G.inverse()*i;
    
    // assign result back to each node
    index = 1;
    for (auto& node: nodes) {
        if (!node.isGround) {
            // set the current vector to 0
            node.voltage = result.get(index);
            index++;
        }
    }
};

void Circuit::draw() {
    
}
