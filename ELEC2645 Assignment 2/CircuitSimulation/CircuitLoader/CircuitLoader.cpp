//
//  CircuitLoader.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 26/11/2021.
//

#include "CircuitLoader.hpp"
#include "FileLoader.hpp"
#include <iostream>
#include "Circuit.hpp"
#include "Node.hpp"
#include "Resistor.hpp"

CircuitLoader::CircuitLoader(const char* fileName, Circuit& circuit): FileLoader(fileName), circuit(circuit) {
    // initialise circuit map with the file size
    this->circuit.map.initialise(this->columns, this->rows);

    // MARK: 1. Identify Nodes
    this->identifyNodes();
    // MARK: 2. Identify Resistors
    this->identifyResistors();
    // MARK: 3. Find the ground/reference node
    this->findGround();
    // MARK: 3. Find the power
    this->findPower();
}


void CircuitLoader::identifyNodes() {
    // Iterate over each item and row in the whole file
    for (int row = 0; row < this->rows; row++) {
        for (int column = 0; column < this->columns; column++) {
            
            // Check wether content of the file was already analysed, if not continue
            CircuitElement* c = this->circuit.map.get(column, row);
            if (c == nullptr) {
                
                char activeChar = this->getCell(column, row);
               
                if (activeChar == '-' || activeChar == '+') {
                    // create new node and add it to circuit elements
                    this->circuit.nodes.push_back(Node());
                    
                    // Interate over whole node branch and identify its components;
                    this->node(column, row, AnalysisDirection::RIGHT, this->circuit.nodes.back());
                }
            }
        }
    }
}

// Resistors must be surrounded by only dashes!
// NOTE: will break if there are any dots after resistors
void CircuitLoader::identifyResistors() {
    // Iterate over each item and row in the whole file
    for (int row = 0; row < this->rows; row++) {
        for (int column = 0; column < this->columns; column++) {
            
            // Check wether content of the file was already analysed, if not continue
            CircuitElement* c = this->circuit.map.get(column, row);
            if (c == nullptr) {
                
                char activeChar = this->getCell(column, row);
               
                if (activeChar == 'R' || activeChar == 'r') {
                    
                    // create new node and add it to circuit map
                    this->circuit.resistors.push_back(Resistor(0));
                    Resistor& currentResistor = this->circuit.resistors.back();
                    
                    this->circuit.map.put(&currentResistor, column, row);
                    
                    // Look for the next characters which are the values of resistors
                    int next = 0;
                    char digit;
                    std::string value = "";
                    do {
                        next++;
                        digit = this->getCell(column + next, row);
                        if (std::isdigit(digit)) {
                            value += std::string(1,digit);
                            // add resistor to circuit map
                            this->circuit.map.put(&currentResistor, column+next, row);
                        }
                    } while (std::isdigit(digit));
                    
                    // Check whether resistor has proper format.
                    // TODO: make more robust
                    assert(("Incorect resistor values", !value.empty()));
                    
                    
                    // Set the resistor proper resistance
                    int resistance = std::stoi(value);
                    // create new node and add it to circuit elements
                    this->circuit.resistors.back().resistance = resistance;
                    
                    // Does dynamic cast without consideeration
                    CircuitElement* left = this->circuit.map.get(column-1, row);
                    if(left != nullptr && left->type == CircuityElementType::NODE){
                        this->circuit.resistors.back().left = dynamic_cast<Node*>(left);
                    }
                    
                    // Does dynamic cast without consideeration
                    CircuitElement* right = this->circuit.map.get(column+next, row);
                    if(right != nullptr && right->type == CircuityElementType::NODE){
                        this->circuit.resistors.back().right = dynamic_cast<Node*>(right);
                    }
                }
            }
        }
    }
}

void CircuitLoader::node(int x, int y, AnalysisDirection direction, Node& node) {
    // get current char in the array
    char activeChar = this->getCell(x, y);
    
    // check wether the part of circuit was already analised.
    // if there is no pointer at specified map addres the node part was not analysed
    if ( this->circuit.map.get(x, y) != nullptr) {
        // already analysed
        return;
    }
    
    
    // swithc for all cases of that node
    switch (activeChar) {
        case '+':
            this->circuit.map.put(&node, x, y);
            
            // branch of recursive search for joints in all directions
            this->node(x, y-1, AnalysisDirection::UP, node);
            this->node(x, y+1, AnalysisDirection::DOWN, node);
            this->node(x-1, y, AnalysisDirection::LEFT, node);
            this->node(x+1, y, AnalysisDirection::RIGHT, node);
            return;
            
        case '|':
            if (direction == AnalysisDirection::UP) {
                this->circuit.map.put(&node, x, y);
                this->node(x, y-1, direction, node);
            } else if (direction == AnalysisDirection::DOWN) {
                this->circuit.map.put(&node, x, y);
                this->node(x, y+1, direction, node);
            }
            return;
    
        case '-':
            if (direction == AnalysisDirection::LEFT ) {
                this->circuit.map.put(&node, x, y);
                this->node(x-1, y, direction, node);
                break;
            } else if (direction == AnalysisDirection::RIGHT) {
                this->circuit.map.put(&node, x, y);
                this->node(x+1, y, direction, node);
            }
            return;

        default:
            return;
    }
}

void CircuitLoader::findGround() {
    // Iterate over each item and row in the whole file
    for (int row = 0; row < this->rows; row++) {
        for (int column = 0; column < this->columns; column++) {
            
            // Check wether content of the file was already analysed, if not continue
            CircuitElement* c = this->circuit.map.get(column, row);
            if (c == nullptr) {
                // check wether ground was found
                if (this->getCell(column, row) == 'G' && this->getCell(column+1, row) == 'N' && this->getCell(column+2, row) == 'D') {
                    // Does dynamic cast without consideeration
                    CircuitElement* node = this->circuit.map.get(column-1, row);
                    if(node != nullptr && node->type == CircuityElementType::NODE){
                        // make node a ground node
                        dynamic_cast<Node*>(node)->isGround = true;
                        // prevent assert from happening
                        return;
                    }
                }
            }
        }
    }
    
    assert(("Ground Node not found or not connected!", false));
}


// Resistors must be surrounded by only dashes!
// NOTE: will break if there are any dots after resistors
void CircuitLoader::findPower() {
    // track wehter power node was found;
    bool found = false;
    
    // Iterate over each item and row in the whole file
    for (int row = 0; row < this->rows; row++) {
        for (int column = 0; column < this->columns; column++) {
            
            // Check wether content of the file was already analysed, if not continue
            CircuitElement* c = this->circuit.map.get(column, row);
            if (c == nullptr) {
                
                char activeChar = this->getCell(column, row);
               
                if (activeChar == 'V' || activeChar == 'v') {
                    // Look for the next characters which are the values of the power source
                    int next = 0;
                    char digit;
                    std::string value = "";
                    do {
                        next++;
                        digit = this->getCell(column + next, row);
                        if (std::isdigit(digit)) {
                            value += std::string(1,digit);
                        }
                    } while (std::isdigit(digit));
                    
                    // Check whether resistor has proper format.
                    // TODO: make more robust
                    assert(("Incorect power value", !value.empty()));
                    
                    
                    // Set the resistor proper resistance
                    int voltage = std::stoi(value);
               
                    // Get the node connected to power
                    CircuitElement* right = this->circuit.map.get(column+next, row);
                    if(right != nullptr && right->type == CircuityElementType::NODE){
                        dynamic_cast<Node*>(right)->isPower = true;
                        dynamic_cast<Node*>(right)->voltage = voltage;
                        found = true;
                    }
                }
            }
        }
    }

    assert(("Power not found or not connected!", found));

}
