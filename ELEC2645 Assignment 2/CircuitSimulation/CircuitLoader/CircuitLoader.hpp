//
//  CircuitLoader.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 26/11/2021.
//

#ifndef CircuitLoader_hpp
#define CircuitLoader_hpp

#include <stdio.h>
#include "FileLoader.hpp"
#include "Circuit.hpp"
#include "Coordinate.hpp"
#include "Node.hpp"




/// Node analysis direction
///
/// Defines which neighbouring elemnt is going to be fetched next
enum class AnalysisDirection: int {
    UP = 1,
    DOWN = 2,
    LEFT = 3,
    RIGHT = 4
};


// TODO: Converto to static class

/// This class loads a circuit
class CircuitLoader: public FileLoader {
private:
    /// Iterates over every cell in the circuit file. To find where a node resides then it recursively searches that branch
    void identifyNodes();
    
    /// Iterates over every cell in the circuit file to find resistors and add their neighbours

    void identifyResistors();
    
    /// Recursive function which is identifying a node
    /// @param x  x position to do the search
    /// @param y  y position to do the search
    /// @param direction  direction in which the identification should move
    /// @param node  curent node reference that is being identified
    void node(int x, int y, AnalysisDirection direction, Node& node);
    
    /// Finds the node connected to ground
    ///
    /// Then sets the node to be ground node. This node is going to be ignored in circuit simulation matrix
    void findGround();
    
    /// Finds the node connected to power
    ///
    /// Then sets the node to be power node
    void findPower();

    // Reference of the circuit where data is being loaded
    Circuit& circuit;
    
public:
    /// Constructs the cicruit from specified file
    /// @param fileName  file to construct cirtuit
    /// @param circuit  destination circuit object where the data is going to be loaded to
    CircuitLoader(const char* fileName, Circuit& circuit);
};

#endif /* CircuitLoader_hpp */
