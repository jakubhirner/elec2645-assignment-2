//
//  Node.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 26/11/2021.
//

#ifndef Node_hpp
#define Node_hpp

#include <stdio.h>
#include "CircuitElement.hpp"


/// Circuit node that represents uni potential circuit points
class Node: public CircuitElement {
public:
    Node();
    bool isGround;
    bool isPower;
    
    float voltage;
    
    // not a nice design but helps with analysis
    int order;
};


#endif /* Node_hpp */
