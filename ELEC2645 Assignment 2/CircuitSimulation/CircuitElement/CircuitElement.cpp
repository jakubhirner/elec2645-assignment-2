//
//  CircuitElement.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 29/11/2021.
//

#include "CircuitElement.hpp"

int CircuitElement::nextID = 0;

CircuitElement::CircuitElement(CircuityElementType type): type(type) {
    CircuitElement::nextID++;
    this->id = CircuitElement::nextID;
}
