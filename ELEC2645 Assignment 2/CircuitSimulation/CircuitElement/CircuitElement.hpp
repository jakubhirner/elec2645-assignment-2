//
//  CircuitElement.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 29/11/2021.
//

#ifndef CircuitElement_hpp
#define CircuitElement_hpp

enum class CircuityElementType: int {
    GROUND = 1,
    VOLTAGE_SOURCE = 2,
    NODE = 3,
    RESISTOR = 4
};


class CircuitElement {
public:
    int id;
    CircuityElementType type;
    CircuitElement(CircuityElementType type);
    virtual ~CircuitElement() = default;
private:
    static int nextID;
};


#endif /* CircuitElement_hpp */
