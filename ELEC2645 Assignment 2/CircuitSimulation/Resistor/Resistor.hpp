//
//  Resistor.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 30/11/2021.
//

#ifndef Resistor_hpp
#define Resistor_hpp

#include "CircuitElement.hpp"
#include "Node.hpp"

/// Class modeling a resistor in a resistor network
///
/// Can be only conected to 2 nodes. These nodes must be diferent or the thing will fail
class Resistor: public CircuitElement {
public:
    int resistance;
    Resistor(int resistance);
    // neighbours
    Node* right;
    Node* left;
public:
    /// Returns conductance of resistor whith is reciprocal of resistance
    float getConductance();
    
    /// Returns the voltage drop accros resistor
    float getVoltage();
    
    /// Gets current flowing through resistor
    float getCurrent();
};


#endif /* Resistor_hpp */
