//
//  Resistor.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 30/11/2021.
//

#include "Resistor.hpp"
#include "CircuitElement.hpp"

Resistor::Resistor(int resistance): CircuitElement(CircuityElementType::RESISTOR), resistance(resistance), right(nullptr), left(nullptr) {}


float Resistor::getConductance() {
    return 1.0f / this->resistance;
}


float Resistor::getVoltage() {
    float voltageLeft = this->left->voltage;
    float voltageRight = this->right->voltage;
    
    return voltageLeft - voltageRight;
}

float Resistor::getCurrent() {
    return this->getVoltage() / this->resistance;
    
}
