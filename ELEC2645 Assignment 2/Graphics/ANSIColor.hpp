//
//  ANSIColor.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 24/11/2021.
//

#ifndef ANSIColor_hpp
#define ANSIColor_hpp

/// Enum containing basic ANSII color values for simple encoding.
///
/// NOTE this value can not be printed on its own since it does not contain proper excape sequence
enum class ANSIColor: unsigned int {
    // Foreground colors
    F_BLACK = 30,
    F_RED = 31,
    F_GREEN = 32,
    F_YELLOW = 33,
    F_BLUE = 34,
    F_MAGENTA = 35,
    F_CYAN = 36,
    F_WHITE = 37,
    F_DEFAULT = 39,
    
    // Background Colors
    B_BLACK = 40,
    B_RED = 41,
    B_GREEN = 42,
    B_YELLOW = 43,
    B_BLUE = 44,
    B_MAGENTA = 45,
    B_CYAN = 46,
    B_WHITE = 47,
    B_DEFAULT = 49,
    
    // means color is not implemented
    NONE = 0
};
#endif /* ANSIColor_hpp */

