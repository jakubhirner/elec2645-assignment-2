//
//  GraphicsElement.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 25/11/2021.
//

#ifndef GraphicsElement_hpp
#define GraphicsElement_hpp

#include "Coordinate.hpp"
#include "Screen.hpp"

/// Abstract class which acts as a template for stuff that has to be drawn on the screen
///
/// Every single drawable thing should inherit from this
class GraphicsElement {
protected:
    /// Position of the element
    ///
    /// If is more than one character wide use centroid as the top left as an anchor
    Coordinate position;
    
    /// Reference to the screen where items should be drawn
    Screen& screen;

public:
    /// Initialise GraphicsElement with required values
    ///
    /// @param x  x position of the element
    /// @param y y position of the element
    /// @param screen reference to the screen where items will be drawn when draw() is calleds
    GraphicsElement(int x, int y, Screen& screen);
    
    /// Function used to draw objects on screen
    ///
    /// Each subclass must implement this
    virtual void draw() = 0;
    
    /// Returns the position of specified graphics element
    const Coordinate getPosition();
};

#endif /* GraphicsElement_hpp */
