//
//  GraphicsElement.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 25/11/2021.
//

#include "GraphicsElement.hpp"
#include "Coordinate.hpp"

GraphicsElement::GraphicsElement(int x, int y, Screen& screen): position(x, y), screen(screen) {}
  
const Coordinate GraphicsElement::getPosition() {
    return this->position;
}
