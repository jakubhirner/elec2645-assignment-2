//
//  InputHandler.cpp
//  ELEC1620
//
//  Created by Jakub Hirner on 28/03/2021.
//

#include "InputHandler.hpp"
#include <iostream>
#include <string>
#include <sys/ioctl.h>



InputHandler::InputHandler() {
    system("stty -echo"); // to prevent user seeing what he types back
    system("stty raw"); // to disable terminal window buffering
    
    this->key = Key::UNDEFINED;
}

InputHandler::~InputHandler() {
    system("stty echo");
    system("stty cooked");
    fflush(stdout); // flush so that changes take place
}

int InputHandler::keyboardHit() {
    static const int STDIN = 0;
    int bytesWaiting;
    
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}

void InputHandler::getInput() {
    char pressed[10]; // maximum number of ansi escape sequence characters premitted by the paplication
    int bytesWaiting = keyboardHit();
    
    for (int byte = 0; byte < bytesWaiting; byte++){
        pressed[byte] =  (char) getchar();
    }

    // checks wether the input is an ANSI escape sequence
    if (pressed[0] == '\e') {
        // if is only 1 byte long it must be escape
        if (bytesWaiting == 1) {
            key = Key::ESC;
            return;
        }
        
        // else it checks wether the length is 3 and next byte is '[' this means it can decode ansi sequene and it is decoded
        // else it is undefined
        if (pressed[1] == '[' && bytesWaiting == 3){
            switch (pressed[2]) {
                case 'A':
                    key = Key::UP;
                    break;
                case 'B':
                    key = Key::DOWN;
                    break;
                case 'C':
                    key = Key::RIGHT;
                    break;
                case 'D':
                    key = Key::LEFT;
                    break;
                default:
                    key = Key::UNDEFINED;
                    break;
            }
        } else {
            key = Key::UNDEFINED;
        }

    } else {
        // if character is not defined in the keys enum returns the ascii value of that character
        key = static_cast<Key>(toupper((char) pressed[0]));
    }
}

/*
void InputHandler::getAsynchInput() {
   std::thread thread(&InputHandler::getInput,this);
   if (thread.joinable()){
       thread.join();
   }
}
*/
