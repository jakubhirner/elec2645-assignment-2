//
//  InputHandler.hpp
//  ELEC1620
//
//  Created by Jakub Hirner on 28/03/2021.
//

#ifndef INPUTHANDLER
#define INPUTHANDLER

//#include <thread>
#include <cstdlib>


/// List of recognised chararacters for specific application
enum class Key: char {
    // arrow keys
    // uses negative values to make sure it won't collide with ascii values
    UP = -1,
    DOWN = -2,
    LEFT = -3,
    RIGHT = -4,

    Z = 'Z', // zooming out
    U = 'U', // zooming in
    
    QUIT = 'Q', // Quit character
    
    ESC = -5,

    UNDEFINED = 0 // Represents undefined escape sequence
};


/// Manages the input from the user.
class InputHandler {
private:
    /// Returns number of waiting bytes to be read from STDIN
    int keyboardHit();
    
public:
    /// Last key pressed from the user
    Key key;
    
    /// Initializes the terminal application for input without echo and need for enter
    InputHandler();
    
    /// Restores terminal to default behaviour
    ~InputHandler();
    
    /// Gets the user input and stores the value in KEY property of the instance
    ///
    /// This either contains defined values in enum KEY or uppercased ASCII character
    void getInput();
    
    //std::atomic<Key> key;
    //void getAsynchInput();
};

#endif
