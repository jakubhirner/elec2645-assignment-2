//
//  Coordinate.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 25/11/2021.
//

#ifndef Coordinate_hpp
#define Coordinate_hpp

// TODO: operator overloading

/// Simple structure containing only x and y integer coordinates
struct Coordinate {
    // x position
    int x;
    
    // y position
    int y;
    
    /// Creates coordinate class
    ///
    /// @param x  x position
    /// @param y  y position
    Coordinate(int x, int y);
};

#endif /* Coordinate_hpp */
