//
//  Text.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 01/12/2021.
//

#include "Text.hpp"
#include "Screen.hpp"
#include "GraphicsElement.hpp"
#include "ANSIColor.hpp"
#include <iostream>

Text::Text(int x, int y, std::string text,  Screen& screen): GraphicsElement(x, y, screen), text(text), background(ANSIColor::NONE), foreground(ANSIColor::NONE) {}

void Text::draw() {
    int offset = 0;
    for(char& c : this->text) {
        this->screen.put(c, this->position.x + offset, this->position.y, this->foreground, this->background);
        offset++;
    }
}
