//
//  Text.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 01/12/2021.
//

#ifndef Text_hpp
#define Text_hpp

#include "GraphicsElement.hpp"
#include "Screen.hpp"
#include <iostream>
#include "ANSIColor.hpp"

// TODO: Document properly
class Text: public GraphicsElement {
public:
    std::string text;
    ANSIColor background;
    ANSIColor foreground;
    Text(int x, int y,std::string text, Screen& screen);
    void draw() override;
};

#endif /* Text_hpp */
