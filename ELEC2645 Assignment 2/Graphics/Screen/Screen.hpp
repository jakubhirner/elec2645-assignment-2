//
//  Screen.hpp
//  ELEC1620
//
//  Created by Jakub Hirner on 23/03/2021.
//

#ifndef SCREEN
#define SCREEN

#include "ANSIColor.hpp"
#include "Coordinate.hpp"
#include "Pixel.hpp"

/// Manages printing out and formatting of the terminal windows
///
/// This class will only work properly if the command line application supports ANSI escape sequences
class Screen {
private:
    /// number of ASCII characters that will fill the full screen
    int size;

    /// Aspect ratio of the screen calculated from width and height
    float aspectRatio;
    
    /// Defines wether cursor is visible or not
    bool cursorIsVisible;
    
    /// foreground color of the screen
    ///
    /// This color will be used if pixel has set color to none
    ANSIColor foregroundColor;
    
    /// backgrund color of the screen
    ///
    /// This color will be used if pixel has set color to none
    ANSIColor backgroundColor;
    
public:
    /// Width of the screen in ASCII characters
    int width;
    
    /// Height of the screen in ASCII characters
    int height;
    
    
    // TODO: use fiend class to allow buffer to be hidden
    /// Data that will be printed to the screen when render() is called
    Pixel* buffer;
    
private:
    /// Changes terminall window size based on stored properties (width, height).
    ///
    /// Uses ANSI excape sequences to accomplish this
    void setScreenSize();
    
    /// Sets the current drawing color to specified value by parameter
    ///
    /// @param color  defiens what color should be drawn
    void setColor(ANSIColor color);

public:
    /// Initializes screen size with specified width and height
    ///
    /// @param width width of the screen in ASCII characters
    /// @param height height of the screen in ASCII characters
    ///
    /// This function initializes screen buffer as well as sets the size property. Lastly it changes the terminal window size
    Screen(int width, int height);
    
    /// Frees the buffer from heap and returns screen to original formatting
    ~Screen();
    
    /// Outputs the screen buffer to terminal window.
    ///
    /// Uses ANSI escape sequences to return cursor at the 0,0 index
    /// Each buffer cell contains single pixel, this pixel is going to be printed with proper formatting
    void render();
    
    /// Clears the buffer
    void clearBuffer();

    /// Shows or hides cursor using ANSI excape sequence
    ///
    /// @param show  sets wether cursor should be visible or not
    void showCursor(bool show);
    
    /// Toggles cursor visibility
    void toggleCursorVisibility();
    
    
    // TODO: remake the ansi coloring to allow default values since it is hideous
    /// Places character on a specific position on a screen
    ///
    /// @param character  the caracter to be drawn at specified location
    /// @param position  location where the character is being placed
    /// @param foregroundColor sets the foreground color gor this character to this specified value
    /// @param backgroundColor  sets the background color gor this character to this specified value
    ///
    /// Performs basich checks to ensure position is not out of bounds
    void put(char character, Coordinate position, ANSIColor foregroundColor = ANSIColor::NONE, ANSIColor backgroundColor= ANSIColor::NONE);
    
    
    /// Places character on a specific position on a screen
    ///
    /// @param character  the caracter to be drawn at specified location
    /// @param x  x position where the character is being placed
    /// @param y  y position where the caracter is being placed
    /// @param foregroundColor  sets the foreground color gor this character to this specified value
    /// @param backgroundColor  sets the foreground color gor this character to this specified value
    ///
    /// Performs basich checks to ensure position is not out of bounds. For out of bounds drawings doesn't draw them
    void put(char character, int x, int y, ANSIColor foregroundColor = ANSIColor::NONE, ANSIColor backgroundColor = ANSIColor::NONE);
    
    /// Clears the screen
    ///
    /// This is done by calling clearBuffer() and then render()
    void clear();
};

#endif
