//
//  Screen.cpp
//  ELEC1620
//
//  Created by Jakub Hirner on 23/03/2021.
//

#include "Screen.hpp"
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "Pixel.hpp"


Screen::Screen(int width, int height) {
    
    // exit function if screen dimensions are too small
    if (width < 1 || height < 1) {
        printf("Screen dimensions are too small");
        std::exit(1);
    }
    
    this->width = width;
    this->height = height;
    size = width * height;
    aspectRatio = width / height;
    
    // make sure cursor is hidden
    this->showCursor(false);
    
    // initialise buffer to be empty
    buffer = new Pixel[size];
    //memset(buffer, ' ', size);
    
    // TODO: make available through API
    // set foreground and background color
    this->foregroundColor = ANSIColor::F_RED;
    this->backgroundColor = ANSIColor::B_BLACK;
    
    // Resize terminal window
    this->setScreenSize();
}

Screen::~Screen() {
    // set formatting to default values
    this->foregroundColor = ANSIColor::F_DEFAULT;
    this->backgroundColor = ANSIColor::F_DEFAULT;
    
    printf("\e[0m");// return view formatting to default settings
    fflush(stdout); // flush buffer
    
    // make cursor visible
    this->showCursor(true);
    
    // delete buffer from heap
    if (this->buffer != nullptr){
        this->clear(); // clear the screen so that it is empty with no formatting
        delete [] this->buffer;
        this->buffer = nullptr;
    }
}

void Screen::setScreenSize() {
    // use ANSI escape sequence to change terminal size in terms of characters
    // NOTE: this does not work in replit
    printf("\e[8;%d;%dt", this->height, this->width);
    fflush(stdout);
}

void Screen::render() {
    
    // this is the default value that will be drawn
    this->setColor(this->foregroundColor);
    this->setColor(this->backgroundColor);
    // iterate for every item in the buffer
    for (int i = 1; i < this->size + 1; i++) {
        
        if (this->buffer[i - 1].foregroundColor == ANSIColor::NONE && this->buffer[i - 1].backgroungColor == ANSIColor::NONE) {        // if NONE is set then ignore color and just print default character
            putchar(this->buffer[i - 1].character);
        } else if (this->buffer[i - 1].foregroundColor != ANSIColor::NONE && this->buffer[i - 1].backgroungColor != ANSIColor::NONE) {
            // if both options are set than print the char and than change to original background
            this->setColor(this->buffer[i - 1].foregroundColor);
            this->setColor(this->buffer[i - 1].backgroungColor);
            putchar(this->buffer[i - 1].character);
            this->setColor(this->foregroundColor);
            this->setColor(this->backgroundColor);
        } else if (this->buffer[i - 1].foregroundColor != ANSIColor::NONE) {
            // if only foreground change that color
            this->setColor(this->buffer[i - 1].foregroundColor);
            putchar(this->buffer[i - 1].character);
            this->setColor(this->foregroundColor);
        } else {
            // if only foreground change that color
            this->setColor(this->buffer[i - 1].backgroungColor);
            putchar(this->buffer[i - 1].character);
            this->setColor(this->backgroundColor);
        }
        
        
        // if is at the end of line print goes to new line
        if (i % (this->width) == 0 && i != this->size) {
            printf("\r\n");
        }
    }

    printf("\e[%dF", this->height - 1);// put cursor to the beggining of the screen
    fflush(stdout); // flush the last buffered text to terminal including the position of the cursor
}

void Screen::clearBuffer(){
    for (int i = 0; i < this->size; i++) {
        // clear each pixel
        this->buffer[i].clear();
    }
}

void Screen::clear(){
    this->clearBuffer();
    this->render();
}

void Screen::showCursor(bool show) {
    if (show) {
        printf("\e[?25h"); // make cursor visible
        this->cursorIsVisible = true;
    } else {
        printf("\e[?25l"); // make cursor hidden
        this->cursorIsVisible = false;
    }
    fflush(stdout);
}

void Screen::toggleCursorVisibility() {
    this->cursorIsVisible = !this->cursorIsVisible;
    this->showCursor(this->cursorIsVisible);
}

void Screen::setColor(ANSIColor color) {
    printf("\e[%im", static_cast<int>(color)); // set foreground color
    fflush(stdout);
}

void Screen::put(char character, Coordinate position, ANSIColor foregroundColor, ANSIColor backgroundColor) {
    this->put(character, position.x, position.y, foregroundColor, backgroundColor);
}

void Screen::put(char character, int x, int y, ANSIColor foregroundColor, ANSIColor backgroundColor) {
    // if x is out of screen then skip the drawing
    if (x < 0 || x > this->width - 1) {
        return;
    }
    
    // if y is out of screen then skip the drawing
    if (y < 0 || y > this->height - 1) {
        return;
    }
    
    // after checks passed put character to proper position in the buffer
    this->buffer[y * this->width + x].character = character;
    this->buffer[y * this->width + x].foregroundColor = foregroundColor;
    this->buffer[y * this->width + x].backgroungColor = backgroundColor;
}
