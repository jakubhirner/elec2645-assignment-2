//
//  Graphics.hpp
//  ELEC2645 Assignment 2
//
//  Created by Jakub Hirner on 01/12/2021.
//

#ifndef Graphics_hpp
#define Graphics_hpp

// This is the include file for all graphics inclusion
// it is sufficient to only include this file to use graphics classes
#include "GraphicsElement.hpp"
#include "Coordinate.hpp"
#include "Cursor.hpp"
#include "Pixel.hpp"
#include "InputHandler.hpp"
#include "Screen.hpp"
#include "ANSIColor.hpp"
#include "Text.hpp"

#endif /* Graphics_hpp */
