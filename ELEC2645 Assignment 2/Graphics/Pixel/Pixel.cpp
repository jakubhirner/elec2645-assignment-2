//
//  Pixel.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 25/11/2021.
//

#include "Pixel.hpp"

void Pixel::clear() {
    this->character = ' ';
    this->formatting = ' ';
    this->backgroungColor = ANSIColor::NONE;
    this->foregroundColor = ANSIColor::NONE;
}
