//
//  Pixel.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 25/11/2021.
//

#ifndef Pixel_hpp
#define Pixel_hpp

#include <stdio.h>
#include "ANSIColor.hpp"

/// Pixel is a single ASCII character containing formatting information to allow colorful drawing
struct Pixel {
    /// Represents the ASCII character that is going to be drawn
    char character = ' ';
    
    // TODO: convert formatting to separate class/struct
    /// formatting of the character (ie bold, italic...)
    char formatting = ' '; // defines text formatting
    
    /// background color of the pixel
    ANSIColor backgroungColor = ANSIColor::NONE;
    
    /// Foreground of the pixel
    ANSIColor foregroundColor = ANSIColor::NONE;
    
    /// Clears pixel values and makes them default
    void clear();
};

#endif /* Pixel_hpp */
