//
//  Cursor.hpp
//  ELEC2645 Assignment 2
//
//  Created by Jakub Hirner on 25/11/2021.
//

#ifndef Cursor_hpp
#define Cursor_hpp

#include "GraphicsElement.hpp"
#include "Screen.hpp"

// TODO: Document properly
class Cursor: public GraphicsElement {
private:
    const char symbol = '+';
public:
    Cursor(int x, int y, Screen& screen);
    
    void moveUp(int steps = 1);
    void moveDown(int steps = 1);
    void moveRight(int steps = 1);
    void moveLeft(int steps = 1);
    
    void draw() override;
};

#endif /* Cursor_hpp */
