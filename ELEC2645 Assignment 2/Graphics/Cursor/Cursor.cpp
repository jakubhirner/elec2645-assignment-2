//
//  Cursor.cpp
//  ELEC2645 Assignment 2
//
//  Created by Jakub Hirner on 25/11/2021.
//

#include "Cursor.hpp"
#include "Screen.hpp"
#include "GraphicsElement.hpp"
#include "ANSIColor.hpp"


Cursor::Cursor(int x, int y, Screen& screen): GraphicsElement(x, y, screen) {}

void Cursor::draw() {
    this->screen.put(this->symbol, this->position, ANSIColor::F_GREEN);
}

void Cursor::moveUp(int steps) {
    // move by specified amount
    this->position.y -= steps;
    
    // check if is not out of screen if so clip to screen bounds
    if (this->position.y < 0) {
        this->position.y = 0;
    }
}

void Cursor::moveDown(int steps) {
    // move by specified amount
    this->position.y += steps;
    
    // check if is not out of screen if so clip to screen bounds
    if (this->position.y > this->screen.height - 1) {
        this->position.y = this->screen.height - 1;
    }
}

void Cursor::moveRight(int steps) {
    // move by specified amount
    this->position.x += steps;
    
    // check if is not out of screen if so clip to screen bounds
    if (this->position.x > this->screen.width - 1) {
        this->position.x = this->screen.width - 1;
    }
}

void Cursor::moveLeft(int steps) {
    // move by specified amount
    this->position.x -= steps;
    
    // check if is not out of screen if so clip to screen bounds
    if (this->position.x < 0) {
        this->position.x = 0;
    }
}
