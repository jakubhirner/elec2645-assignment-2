//
//  FileLoader.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 26/11/2021.
//

#ifndef FileLoader_hpp
#define FileLoader_hpp

#include <vector>
#include <string>
// TODO: Converto to static class

/// Loads the file into file data buffer
///
/// Not ideal but there is ont enough time to do nice api. The focus is now on functionality
struct FileLoader {

    
    int rows;
    int columns;
    int size;

    /// Loads the file into internal character buffer
    /// @param fileName name of the file to be loaded
    ///
    /// File is loaded into internal vector each element in vector contains a separate line
    static void loadFile(const char* fileName);
    
    char getCell(int column, int row);
    
    
    /// Initializes file from file
    /// @param fileName file path of .obj file to be loaded, requires terminal window to be in directory where the file is present
    FileLoader(const char* fileName);
    
private:
    /// Data containing all data of the file in buffer stored in HEAP
    ///
    /// data can only be read by getCell
    char* fileData;
};


#endif /* FileLoader_hpp */
