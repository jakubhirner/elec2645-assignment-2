//
//  FileLoader.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 26/11/2021.
//

#include "FileLoader.hpp"
#include <vector>
#include <fstream>
#include <iostream>


// TODO: Add proper deconstructors
// TODO: Completely redo
// -- the problem I am having is that I don't know exactly how the api should work hence it is hard to desig it properly form the get go
FileLoader::FileLoader(const char* fileName) {
    std::ifstream file;
    file.open(fileName);
    
    // force quit if file is not found
    assert(("file not found",file.is_open()));
    
    std::vector<std::string> lines;

    // initialise vector of strings
    // breaks after empty line. there may be only one separate circuit
    while (!file.eof()){
        std::string line;
        getline(file, line);
        
        //TODO: allow first line to be negative
        // exit if empty line is found
        if (line.length() == 0) {
            break;
        }
        lines.push_back(line);
    }
    
    // find the longest row
    int longest = 0;
    for(std::string& line: lines) {
        int length = (int) line.length();
        longest = length >longest ? length : longest;
    }
    
    // initialise required file data memories
    this->columns = longest;
    this->rows = (int) lines.size();
    this->size = this->columns * this->rows;
    
    // initialise buffer to be empty
    this->fileData = new char[this->size];
    memset(this->fileData, ' ', this->size);
    
    // add the required data to data file content
    int row = 0;
    for(std::string& line: lines) {

        int column = 0;
        for(char& c : line) {
            this->fileData[row * this->columns + column] = c;
            column++;
        }
        row++;
    }
}

char FileLoader::getCell(int column, int row) {
    // if is out of screen return empty
    if (column < 0 || column > this->columns - 1) {
        return ' ';
    }
    
    // if is out of screen return empty
    if (row < 0 || row > this->rows - 1) {
        return ' ';
    }
    
    return this->fileData[row * this->columns + column];
}
