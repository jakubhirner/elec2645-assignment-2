//
//  Vector.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 05/12/2021.
//

#include "Vector.hpp"
#include <string>

Vector::Vector(int size): size(size) {
    this->elements = new float[size];
    for (int i = 0; i < this->size; i++) {
        this->elements[i] = 0.f;
    }
}


Vector::Vector(int size, float* elements): size(size) {
    this->elements = new float[size];
    for (int i = 0; i < size; i++) {
        this->elements[i] = elements[i];
    }
}

Vector::Vector(const Vector &v) {
    this->elements = new float[v.size];
    this->size = v.size;
    for (int i = 0; i < v.size; i++) {
        this->elements[i] = v.elements[i];
    }
}

Vector::~Vector() {
    delete [] this->elements;
    this->elements = nullptr;
}


float Vector::get(int row) const {
    return this->elements[row-1];
}

void Vector::set(int row, float value) {
    this->elements[row-1] = value;
}


void Vector::print() const {
    printf("Size: %i \n\n", this->size);
    
    for (int row = 1; row <= this->size; row++) {
        printf("|\t");
        printf("%f\t", this->get(row));
        printf("|\n");
    }
}
