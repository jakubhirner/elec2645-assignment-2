//
//  Vector.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 05/12/2021.
//

#ifndef Vector_hpp
#define Vector_hpp

/// Mathematical Vector class containing float elements and basic functionality
class Vector {
private:
    /// Heap storage of all the vector members
    float* elements;
public:
    /// Number of vector rows
    int size;
    
    /// Initialises vector of specified size with zeros
    /// @param size  number of vector elements
    Vector(int size);
    
    /// Initialises the vector of specified size with elements form an array
    /// @param size  number of vector elements
    /// @param elements elements to be loaded into the vector
    Vector(int size, float* elements);
    
    /// Overriding provded copy constructor to ensure deep copies
    Vector(const Vector& v);
    
    /// Frees the buffer from heap
    ~Vector();
    
    /// Returns the elemen value on specified ros.
    ///
    /// Indexes start from 1 as is the convention in math
    /// @param row  index of the row for desired element
    float get(int row) const;
    
    /// Writes value to specified element
    /// @param row  index of the row where new value is going to be written
    /// @param value value to be written
    ///
    /// Idexes start from 1 as is the convention in math
    void set(int row, float value);
    
    /// Prints the vector to allow easier debuging
    void print() const;
    
    
    
    // Ignore for now
    /*
    Vector& operator = (const Vector& rhs);
    Vector operator + (const Vector& rhs) const;
    Vector operator - (const Vector& rhs) const;
    Vector operator * (const Vector& rhs) const;
    Vector operator * (float x) const;
    Vector operator / (float x) const;
    bool operator == (Vector& rhs) const;
    */
    
};

#endif /* Vector_hpp */
