//
//  Matrix.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 05/12/2021.
//

#include "Matrix.hpp"
#include "Vector.hpp"
#include <string>
#include <cmath>

// MARK: - Dimensions struct
Dimensions::Dimensions(int rows, int cols) {
    this->rows = rows;
    this->cols = cols;
    this->isSquare = rows == cols;
    this->size = rows * cols;
}

bool Dimensions::operator == (Dimensions const& a) const {
    return ((this->rows == a.rows) && (this->cols == a.cols));
}

void dimensionCheck(Dimensions a, Dimensions b, DimensionRequirement requirement) {
    switch (requirement) {
        case DimensionRequirement::multiplication:
            if (a.cols != b.rows) {
                throw "Multiplication error, matrix dimension mismatch";
            }
            break;
        case DimensionRequirement::subtraction:
            if (!(a == b)) {
                throw "Substraction error, matrix dimension mismatch";
            }
            break;
        case DimensionRequirement::addition:
            if (!(a == b)) {
                throw "Addition error, matrix dimension mismatch";
            }
            break;
        case DimensionRequirement::inverse:
            if (!(a == b)) {
                throw "Inverse error, matrix dimension mismatch";
            }
            break;
        case DimensionRequirement::assignment:
            if (!(a == b)) {
                throw "Assignment error, matrix dimension mismatch";
            }
            break;
    }
}

// MARK: - Matrix class

// MARK: Constructors
Matrix::Matrix(int rows, int cols): dimensions(rows, cols) {
    this->elements = new float[this->dimensions.size];
    for (int i = 0; i < this->dimensions.size; i++) {
        this->elements[i] = 0.f;
    }
}

// MARK: Constructors
Matrix::Matrix(Dimensions dimensions): dimensions(dimensions) {
    this->elements = new float[this->dimensions.size];
    for (int i = 0; i < this->dimensions.size; i++) {
        this->elements[i] = 0.f;
    }
}


Matrix::Matrix(int rows, int cols, float* elements): dimensions(rows, cols) {
    Dimensions dimensions(rows, cols);
    this->dimensions = dimensions;

    this->elements = new float[this->dimensions.size];
    for (int i = 0; i < this->dimensions.size; i++) {
        this->elements[i] = elements[i];
    }
}

Matrix::Matrix(const Matrix &m): dimensions(m.dimensions.rows, m.dimensions.cols) {
    this->dimensions = m.dimensions;

    this->elements = new float[this->dimensions.size];
    for (int i = 0; i < this->dimensions.size; i++) {
        this->elements[i] = m.elements[i];
    }
}

Matrix::~Matrix() {
    delete [] this->elements;
    this->elements = nullptr;
}


// MARK: Methods
int Matrix::mapIndex(int row, int col) const {
    return (row - 1) * this->dimensions.cols + col - 1;
}

float Matrix::get(int row, int col) const {
    return this->elements[this->mapIndex(row, col)];
}

void Matrix::set(int row, int col, float value) {
    this->elements[this->mapIndex(row, col)] = value;
}

float Matrix::get(int index) const {
    return this->elements[index];
}

void Matrix::set(int index, float value) {
    this->elements[index] = value;
}

void Matrix::print() const {
    printf("Size: %i x %i \n\n", this->dimensions.rows, this->dimensions.cols);
    
    for (int row = 1; row <= this->dimensions.rows; row++) {
        printf("|\t");
        for (int col = 1; col <= this->dimensions.cols; col++) {
            printf("%f\t", this->get(row,col));
        }
        printf("|\n");
    }
}


// MARK: Operator overloading

Matrix& Matrix::operator = (const Matrix& rhs) {
    dimensionCheck(this->dimensions, rhs.dimensions, DimensionRequirement::assignment);
    
    for (int i = 0; i < this->dimensions.size; i++) {
        this->set(i,rhs.get(i));
    }
    
    return *this;
}

Matrix Matrix::operator + (const Matrix& rhs) const {
    dimensionCheck(this->dimensions, rhs.dimensions, DimensionRequirement::addition);
    
    Matrix placeholder(this->dimensions.rows, this->dimensions.cols);
    
    for (int i = 0; i < this->dimensions.size; i++) {
        placeholder.set(i, this->get(i) + rhs.get(i));
    }

    return placeholder;
}

Matrix Matrix::operator - (const Matrix& rhs) const {
    dimensionCheck(this->dimensions, rhs.dimensions, DimensionRequirement::subtraction);

    Matrix placeholder(this->dimensions.rows, this->dimensions.cols);
    
    for (int i = 0; i < this->dimensions.size; i++) {
        placeholder.set(i, this->get(i) - rhs.get(i));
    }

    return placeholder;
}

Matrix Matrix::operator * (const Matrix& rhs) const {
    dimensionCheck(this->dimensions, rhs.dimensions, DimensionRequirement::multiplication);

    Matrix placeholder(this->dimensions.rows, rhs.dimensions.cols);
    
    for (int row = 1; row <= this->dimensions.rows; row++) {
        for (int col = 1; col <= rhs.dimensions.cols; col++) {
            float a = 0;
            
            for (int i = 1; i <= rhs.dimensions.rows; i++) {
                a += this->get(row, i) * rhs.get(i, col);
            }
            
            placeholder.set(row, col, a);
        }
    }

    return placeholder;
}

Matrix Matrix::operator * (float x) const {
    Matrix placeholder(this->dimensions.rows, this->dimensions.cols);
    
    for (int i = 0; i < this->dimensions.size; i++) {
        placeholder.set(i, this->get(i) * x);
    }

    return placeholder;
}

Matrix Matrix::operator / (float x) const {
    Matrix placeholder(this->dimensions.rows, this->dimensions.cols);
    
    for (int i = 0; i < this->dimensions.size; i++) {
        placeholder.set(i, this->get(i) / x);
    }

    return placeholder;
}

bool Matrix::operator == (const Matrix& rhs) const {
    if (!(this->dimensions == rhs.dimensions)) {
        return false;
    }
    
    bool equals = true;
    for (int i = 0; i < this->dimensions.size; i++) {
        equals &= (std::fabs(this->get(i) - rhs.get(i)) < 0.000005f);
;
    }
    
    return equals;
}


Vector Matrix::operator * (const Vector& rhs) const{
    Dimensions dimensions(rhs.size,1);
    dimensionCheck(this->dimensions, dimensions, DimensionRequirement::multiplication);

    Vector placeholder(this->dimensions.rows);
    
    for (int row = 1; row <= this->dimensions.rows; row++) {
        float a = 0;
        
        for (int i = 1; i <= rhs.size; i++) {
            a += this->get(row, i) * rhs.get(i);
        }
        
        placeholder.set(row, a);
    }

    return placeholder;
}

// MARK: - Matrix transformatinos

float Matrix::determinant() const {
    // square dimensin check
    if (!this->dimensions.isSquare) {
        throw "cannot compute determinant for nonsquare matrix";
    } else if (this->dimensions.rows == 2) {
        return this->get(1, 1) * this->get(2, 2) - this->get(1, 2) * this->get(2, 1);
    }
    
    int sign = 1; // used to define the minors sign
    float determinant = 0;
    
    // iterate over the whole row in main matrix
    for (int col = 1; col <= this->dimensions.cols; col++) {
        // create new matrix to get elements of
        Matrix newM(this->dimensions.cols - 1, this->dimensions.cols - 1);
        int adjusedCol = 1;
        for(int newCol = 1; newCol < this->dimensions.cols; newCol++) {
            for(int newRow = 1; newRow < this->dimensions.rows; newRow++) {
                if (adjusedCol==col) {
                    adjusedCol++;
                }
                
                float e = this->get(newRow+1, adjusedCol);
                newM.set(newRow, newCol, e);
            }
            adjusedCol++;
        }
        
        // calculate determinant
        determinant += this->get(1, col)*sign*newM.determinant();
        
        sign *= -1; // change minors
    }
    return determinant;
    
}

Matrix Matrix::transpose() {
    Matrix placeholder(this->dimensions.cols, this->dimensions.rows);
    
    // iterate over the whole row and rowin main matrix
    for (int col = 1; col <= this->dimensions.cols; col++) {
        for (int row = 1; row <= this->dimensions.rows; row++) {
            placeholder.set(col, row, this->get(row, col));
        }
    }
    return placeholder;
}

Matrix Matrix::adjoint() {
    // square dimensin check
    if (!this->dimensions.isSquare) {
        throw "cannot compute adjoint for nonsquare matrix";
    }
    

    Matrix placeholder(this->dimensions.rows, this->dimensions.cols);
    
    
    int colSign = 1;
    // iterate over the whole row and column in main matrix
    for (int col = 1; col <= this->dimensions.cols; col++) {
        int rowSign = colSign;
        for (int row = 1; row <= this->dimensions.rows; row++) {
        
            
            // create new matrix to get elements of
            Matrix newM(this->dimensions.cols - 1, this->dimensions.cols - 1);
            
            int adjusedCol = 1;
            for(int newCol = 1; newCol < this->dimensions.cols; newCol++) {
                int adjustedRow = 1;
                if (adjusedCol==col) {
                    adjusedCol++;
                }
                for(int newRow = 1; newRow < this->dimensions.rows; newRow++) {
                    if (adjustedRow==row) {
                        adjustedRow++;
                    }
                    
                    float e = this->get(adjustedRow, adjusedCol);
                    newM.set(newRow, newCol, e);
                    adjustedRow++;
                }
                adjusedCol++;
            }
            
            placeholder.set(row, col, rowSign*(newM.determinant()));
            rowSign *= -1;
        }
        colSign *= -1;
    }

    return placeholder.transpose();
};


// TODO: preliminary implementation so that the api can be tested
Matrix Matrix::inverse() {
    return (this->adjoint() / this->determinant());
}


