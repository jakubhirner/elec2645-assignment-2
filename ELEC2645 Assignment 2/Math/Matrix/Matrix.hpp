//
//  Matrix.hpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 05/12/2021.
//

#ifndef Matrix_hpp
#define Matrix_hpp

#include "Vector.hpp"

struct Dimensions {
    int rows;
    int cols;
    
    bool isSquare;
    int size;
    
    Dimensions(int rows, int cols);
    
    bool operator == (const Dimensions& a) const;
};

enum class DimensionRequirement {
    multiplication,
    addition,
    subtraction,
    inverse,
    assignment
};

void dimensionCheck(Dimensions a, Dimensions b, DimensionRequirement requirement);

class Matrix {
private:
    float* elements;
    int mapIndex(int row, int col) const;
public:
    Dimensions dimensions;
public:
    Matrix(int rows, int cols);
    Matrix(Dimensions dimensions);
    Matrix(int rows, int cols, float* elements);
    Matrix(const Matrix& m);
    
    /// Frees the buffer from heap
    ~Matrix();
    
    float get(int row, int col) const;
    float get(int index) const;
    
    void set(int row, int col, float value);
    void set(int index, float value);
   
    float determinant() const;
    Matrix transpose();
    Matrix adjoint();
    Matrix inverse();
    
    void print() const;
    
    Matrix& operator = (const Matrix& rhs);
    Matrix operator + (const Matrix& rhs) const;
    Matrix operator - (const Matrix& rhs) const;
    Matrix operator * (const Matrix& rhs) const;
    Matrix operator * (float x) const;
    Matrix operator / (float x) const;
    
    
    bool operator == (const Matrix& rhs) const;
    
    // Vector overloading
    Vector operator * (const Vector& rhs) const;
};


#endif /* Matrix_hpp */
