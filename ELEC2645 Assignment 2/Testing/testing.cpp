//
//  testing.cpp
//  ELEC2645-A2
//
//  Created by Jakub Hirner on 05/12/2021.
//

#include "testing.hpp"
#include <iostream>
#include "Math.hpp"
#include <cmath>

bool testingMatrixEqual(const Matrix& a, const Matrix& b, bool expected);
bool testingMatrixAssignment(const Matrix& a);
bool testingMatrixAddition(const Matrix& a, const Matrix& b, const Matrix& expected);
bool testingMatrixMultiplication(const Matrix& a, const Matrix& b, const Matrix& expected);
bool testingMatrixDeterminant(const Matrix& a, float expected);


int test(void) {
    
    std::cout << "---------------------- Testing ----------------------" << std::endl;
    
    float elementsA[] = {1.f,-2.f,3.f,-4.f,5.f,-6.f,7.f,-8.f,-9.f};
    float elementsB[] = {5.f,9.f,-15.f,-45.f,-6.f,6.f,7.f,15.f,-33.f};
    float elementsC[] = {-1.f,2.f,3.f,-4.f,-5.f,6.f,7.f,-8.f,9.1f};


    Matrix a(3, 3, elementsA);
    Matrix b(3, 3, elementsB);
    Matrix c(3, 3, elementsC);

    
    //a.inverse();
    a.inverse().print();

    // Equals
    std::cout << "- Matrix ==" << std::endl;
    testingMatrixEqual(a, a, true);
    testingMatrixEqual(b, a, false);
    testingMatrixEqual(a, b, false);
    testingMatrixEqual(c, a, false);
    testingMatrixEqual(a, c, false);
    testingMatrixEqual(c, c, true);
    testingMatrixEqual(b, b, true);
    std::cout << "" << std::endl;

    
    // assignment
    std::cout << "- Matrix =" << std::endl;
    testingMatrixAssignment(a);
    testingMatrixAssignment(c);
    testingMatrixAssignment(b);
    std::cout << "" << std::endl;

    // Plus
    std::cout << "- Matrix +" << std::endl;
    float elementsANB[] = {6.f,7.f,-12.f,-49.f,-1.f,0.f,14.f,7.f,-42.f};
    float elementsANC[] = {0.f,0.f,6.0000f,-8.0000f,0.f,0.f,14.0000f,-16.0000f,0.1000f};
    float elementsBNC[] = {4.0000,11.0000,-12.0000,-49.0000,-11.0000,12.0000,14.0000,7.0000,-23.9000};
    Matrix aNb(3,3,elementsANB);
    Matrix aNc(3,3,elementsANC);
    Matrix bNc(3,3,elementsBNC);
    testingMatrixAddition(a, b, aNb);
    testingMatrixAddition(a, c, aNc);
    testingMatrixAddition(b, c, bNc);
    std::cout << "" << std::endl;
    /*
    Vector v(3, elementsA);
    (a * v).print();*/
    
    // Determinant
    std::cout << "- Determinant" << std::endl;
    testingMatrixDeterminant(a, 54.0000f);
    testingMatrixDeterminant(b, -2952.0f);
    testingMatrixDeterminant(c, 355.30f);
    std::cout << "" << std::endl;

    
    return 0;

}

bool testingMatrixEqual(const Matrix& a, const Matrix& b, bool expected) {
    bool result = (a == b);  // calc value and compare to expected
    if (result == expected) {
      std::cout << "passed\n";
      return true;
    } else {
      std::cout << "FAILED! " << result << " (expecting " << expected << ").\n";
      return false;
    }
}

bool testingMatrixAssignment(const Matrix& a) {
    Matrix test(a.dimensions);
    
    test = a;
    if (test == a) {
      std::cout << "passed\n";
      return true;
    } else {
      std::cout << "FAILED!\n";
      return false;
    }
}

bool testingMatrixAddition(const Matrix& a, const Matrix& b, const Matrix& expected) {
    Matrix result(a.dimensions);
    
    result = a + b;
    if (result == expected) {
      std::cout << "passed\n";
      return true;
    } else {
      std::cout << "FAILED!\n";
      return false;
    }
}

bool testingMatrixMultiplication(const Matrix& a, const Matrix& b, const Matrix& expected) {
    Matrix result(a.dimensions);
    
    result = a + b;
    if (result == expected) {
      std::cout << "passed\n";
      return true;
    } else {
      std::cout << "FAILED!\n";
      return false;
    }
}

bool testingMatrixDeterminant(const Matrix& a, float expected) {
    if (std::fabs(a.determinant() - expected) < 0.000005f) {
      std::cout << "passed\n";
      return true;
    } else {
      std::cout << "FAILED!\n";
      return false;
    }
}

