//
//  main.cpp
//  ELEC2645 Assignment 2
//
//  Created by Jakub Hirner on 24/11/2021.
//

#include "Graphics.hpp"
#include "CirciutSimulator.hpp"
#include "Math.hpp"

int main(int argc, char *argv[]) {
    
    #if DEBUG
        #include "testing.hpp"
        test();
    #endif
    
    // MARK: - Initialise console

    //system("clear");
    Screen screen(80, 30);
    InputHandler inputHandler;
    
    // MARK: - Initialise Circuit
    Circuit circ(screen);
    // use absolute path for now

    circ.load("/Users/jakubhirner/Documents/University of Leeds/Year 2/ELEC2645/ELEC2645 Assignment 2/ELEC2645 Assignment 2/Circuit.txt");
    circ.simulate();
    CircuitLoader circuit("/Users/jakubhirner/Documents/University of Leeds/Year 2/ELEC2645/ELEC2645 Assignment 2/ELEC2645 Assignment 2/Circuit.txt", circ);
    

    // MARK: - Graphical elements

    bool quit = false;
    bool showNodes = false;
    bool movingCirc = false;
    
    Coordinate circPosition(6,6);
    Cursor cursor(1, 1, screen);

    Text text(3,30-5,"Voltage",screen);
    text.foreground = ANSIColor::F_WHITE;
    Text text2(3,30-4,"Current",screen);
    text2.foreground = ANSIColor::F_WHITE;

    while (!quit) {
        inputHandler.getInput();
        
        // MARK: - User input
        switch (inputHandler.key) {
            case Key::QUIT:
                quit = true;
                break;
            case Key::ESC:
                screen.toggleCursorVisibility();
                break;
            case Key::UP:
                if (movingCirc) {
                    circPosition.y -= 1;
                } else {
                    cursor.moveUp();
                }
                break;
            case Key::DOWN:
                if (movingCirc) {
                    circPosition.y += 1;
                } else {
                    cursor.moveDown();
                }
                break;
            case Key::LEFT:
                if (movingCirc) {
                    circPosition.x -= 1;
                } else {
                    cursor.moveLeft();
                }
                break;
            case Key::RIGHT:
                if (movingCirc) {
                    circPosition.x += 1;
                } else {
                    cursor.moveRight();
                }
                break;
            case Key::Z:
                showNodes = !showNodes;
                break;
            case Key::U:
                movingCirc = !movingCirc;
                break;

            default:
                break;
        }
        
        
        // MARK: - Draw Circuit
        for (int row = 0; row < circuit.rows; row++) {
            for (int column = 0; column < circuit.columns; column++) {
                Coordinate coordinate(column + circPosition.x, row + circPosition.y);

                char c;

                if (showNodes) {
                    char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A','B','C','E','G','R','T','P','W' };
                    
                    auto cc = circ.map.get(column, row);
                    
                    if (cc != nullptr) {
                        if(cc->type == CircuityElementType::RESISTOR) {
                            screen.put('R', coordinate, ANSIColor::F_GREEN, ANSIColor::NONE);
                        } else {
                            if (dynamic_cast<Node*>(cc)->isGround){
                                c = 'g';
                                screen.put(c, coordinate, ANSIColor::F_RED, ANSIColor::NONE);
                            } else if (dynamic_cast<Node*>(cc)->isPower){
                                c = 'p';
                                screen.put(c, coordinate, ANSIColor::F_YELLOW, ANSIColor::NONE);
                            } else {
                                c = digits[cc->id];
                                screen.put(c, coordinate, ANSIColor::F_CYAN, ANSIColor::NONE);
                            }
                        }
                    }
                } else {
                    c = circuit.getCell(column, row);
                    if (c != ' ') {
                        screen.put(c, coordinate, ANSIColor::F_CYAN, ANSIColor::NONE);
                    }
                }
            }
        }
        
        
        // MARK: - Get Data based on cursor and circuit location

        int relativeX = cursor.getPosition().x - circ.getPosition().x - circPosition.x;
        int relativeY = cursor.getPosition().y - circ.getPosition().y - circPosition.y;
        
        CircuitElement* activeElement = circ.map.get(relativeX, relativeY);
        if (activeElement != nullptr) {
            if(activeElement->type == CircuityElementType::RESISTOR) {
                float voltageDrop = dynamic_cast<Resistor*>(activeElement)->getVoltage();
                float current = dynamic_cast<Resistor*>(activeElement)->getCurrent();
                text.text = "Voltage: " + std::to_string(voltageDrop) + " V";
                
                if (current < 0) {
                    text2.text = "Current: " + std::to_string(current * -1) + " A <-";
                } else {
                    text2.text = "Current: " + std::to_string(current) + " A ->";
                }
                
                
                
                text.draw();
                text2.draw();
            } else if (activeElement->type == CircuityElementType::NODE) {
                float voltage = dynamic_cast<Node*>(activeElement)->voltage;
                text.text = "Voltage: " + std::to_string(voltage) + " V";
                text.draw();
            }
        }
        
        // MARK: - Render
        cursor.draw();
        screen.render();
        screen.clearBuffer();
    }

    return 0;
}
